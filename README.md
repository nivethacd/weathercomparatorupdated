# _**Weather Comparator**_

Final assignment for internship at TestVagrant.

UI testing is done on https://www.accuweather.com/ and API testing is done on
https://openweathermap.org/

The temperature from both the websites is taken and stored and is compared with an exception difference of 3.
If the temperature from both the websites are equal or if the difference is lesser than 3 then the test case is passed if not the test case is failed.