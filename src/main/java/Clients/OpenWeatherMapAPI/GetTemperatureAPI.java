package Clients.OpenWeatherMapAPI;

import Utilities.PropertyReader;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Assert;

import java.io.IOException;
import java.util.Properties;

import static io.restassured.RestAssured.given;

public class GetTemperatureAPI {
    public int getTempFromOpenWeatherMap() throws IOException, ParseException {
        Properties properties = new PropertyReader().propertyReader();

        Response response = (Response) given()
                .queryParam("q", properties.getProperty("City"))
                .queryParam("appid",properties.getProperty("apiKey"))
                .queryParam("units", properties.getProperty("unit"))
                .when()
                .get(properties.getProperty("APIpath"));


        JSONParser Parser = new JSONParser();
        Object obj = Parser.parse(response.asString());
        JSONObject JSON = (JSONObject) obj;
        JSONObject tempmain = (JSONObject) JSON.get("main");
        double temp = (double) tempmain.get("temp");
        int APITemperature = (int)temp;

        return APITemperature;
    }
}
