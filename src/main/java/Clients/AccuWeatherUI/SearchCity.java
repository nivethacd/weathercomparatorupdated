package Clients.AccuWeatherUI;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchCity extends PageInitialisation {
    public WebDriver driver;

    public SearchCity(WebDriver driver) {
        super(driver);
        this.driver=driver;
    }

    @FindBy(className = "search-input")
    WebElement searchPlace;

    @FindBy(className = "temp")
    WebElement moreDetails;

    @FindBy(xpath = "//div[@id='dismiss-button']/div")
    WebElement closeAds;

    @FindBy(className = "display-temp")
    WebElement temperature;

    public void SearchPlace(String city){
        searchPlace.sendKeys(city+ Keys.RETURN);
    }
    public void MoreDetails(){
        moreDetails.click();
    }
    public void CloseAds(){
        driver.switchTo().frame("google_ads_iframe_/6581/web/in/interstitial/weather/local_home_0");
        closeAds.click();
    }
    public String FinalTempScreen(){
        return temperature.getText();
    }
}
