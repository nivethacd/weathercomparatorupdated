package Clients.AccuWeatherUI;

import Utilities.BrowserSetup;
import Utilities.PropertyReader;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class CallingUITemp {
    private static WebDriver driver;
    private Properties properties;

    public int callUITemp() throws IOException {
        PropertyReader pr = new PropertyReader();
        properties = pr.propertyReader();
        BrowserSetup browserSetup = new BrowserSetup();
        driver= browserSetup.Browsers();
        driver.manage().window().maximize();
        driver.get(properties.getProperty("UIpath"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        SearchCity searchCity = new SearchCity(driver);
        searchCity.SearchPlace(properties.getProperty("City"));
        searchCity.MoreDetails();
        searchCity.CloseAds();
        int tempUI = Integer.parseInt(searchCity.FinalTempScreen().replaceAll("[^\\d]",""));

        driver.quit();
        return tempUI;
    }

}
