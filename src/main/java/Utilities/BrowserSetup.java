package Utilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.io.IOException;
import java.util.Properties;

public class BrowserSetup {
    WebDriver driver;
    Properties properties;


    public WebDriver Browsers() throws IOException {

        PropertyReader pr = new PropertyReader();
        properties = pr.propertyReader();

        if (properties.getProperty("browser").equals("chrome")) {
            WebDriverManager.chromedriver().setup();
            ChromeOptions opt = new ChromeOptions();
            opt.addArguments("--disable-notifications");
            return driver = new ChromeDriver(opt);
        } else {
            WebDriverManager.firefoxdriver().setup();
            FirefoxOptions opt = new FirefoxOptions();
            opt.addPreference("dom.webnotifications.enable", false);
            return driver = new FirefoxDriver(opt);
        }
    }

}
