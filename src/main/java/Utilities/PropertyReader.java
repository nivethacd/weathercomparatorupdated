package Utilities;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
    public Properties propertyReader() throws IOException {
        FileReader fileReader = new FileReader(System.getProperty("user.dir")+"/src/main/resources/Data.properties");
        Properties properties = new Properties();
        properties.load(fileReader);
        return properties;
    }
}
