import Clients.AccuWeatherUI.CallingUITemp;
import Clients.OpenWeatherMapAPI.GetTemperatureAPI;
import org.json.simple.parser.ParseException;
import org.junit.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class TemperatureComparator {
@Test
    public void weatherComparing() throws IOException, ParseException {
    //UI
    CallingUITemp callingUITemp = new CallingUITemp();
    int UITemperature = callingUITemp.callUITemp();
    System.out.println("The temperature from 'AccuWeather' for the given city is: "+UITemperature+"°C");

    //API
    GetTemperatureAPI getTemperatureAPI = new GetTemperatureAPI();
    int APITemperature = getTemperatureAPI.getTempFromOpenWeatherMap();
    System.out.println("The temperature from 'OpenWeatherMap' for the given city is: "+APITemperature+"°C");

    //Asserting, if the value from both the websites is equal or less than 3, the test case is passed
    Assert.assertTrue(Math.abs(APITemperature - UITemperature)<3);
}
}
